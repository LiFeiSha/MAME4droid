package com.seleuco.config;

/*
* 管理游戏内容的单例
* */
public class GameManager {
    // 要运行的游戏
    public String runningGame = "";
    // 游戏名称
    public String gameName = "";

    // 在自己内部定义一个实例，只在内部调用
    private static volatile GameManager instance = null;
    private GameManager() {}

    // 提供给外部访问classd的静态方法
    public static GameManager getInstance() {
        if (instance == null) {
            synchronized (GameManager.class) {
                if (instance == null) {
                    instance = new GameManager();
                }
            }
        }

        return instance;
    }
}
