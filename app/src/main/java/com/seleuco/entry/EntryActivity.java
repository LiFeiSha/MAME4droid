package com.seleuco.entry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageButton;

import com.seleuco.config.GameManager;
import com.seleuco.mame4all.MAME4all;
import com.seleuco.mame4all.R;
import com.umeng.analytics.MobclickAgent;

public class EntryActivity extends Activity {
    // 单人按钮
    ImageButton singleBtn = null;
    // 退出按钮
    ImageButton backBtn = null;
    // 记录游戏

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // 赋予控件内容
        setContentView(R.layout.activity_entry);
        // 获取按钮
        singleBtn = (ImageButton) findViewById(R.id.single_game);
        backBtn = (ImageButton) findViewById(R.id.exit);

        singleBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                if (motionEvent.getAction() == MotionEvent.ACTION_DOWN) {
                    // 重新设置按下时候的背景图片
                    ((ImageButton) view).setBackgroundDrawable(getResources().getDrawable(R.drawable.single_click));
                } else if (motionEvent.getAction() == MotionEvent.ACTION_UP) {
                    // 再修改抬起时的背景图片
                    ((ImageButton) view).setBackgroundDrawable(getResources().getDrawable(R.drawable.single_normal));
                    // 传入数据，打开模拟器
                    Intent intent = new Intent(EntryActivity.this, MAME4all.class);

                    String game = GameManager.getInstance().runningGame;
                    Log.d("EntryActivity", "游戏 + " + game);

                    intent.putExtra("game", game);
                    startActivity(intent);
                }
                return false;
            }
        });

        backBtn.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {

                Intent intent = new Intent(EntryActivity.this, GameListActivity.class);
                startActivity(intent);
                finish();

                return false;
            }
        });


        String gameName = GameManager.getInstance().gameName;
        MobclickAgent.onEvent(this, "选择了游戏" + gameName);
    }
}
