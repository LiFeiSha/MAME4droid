package com.seleuco.entry;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.seleuco.config.GameManager;
import com.seleuco.mame4all.MAME4all;
import com.seleuco.mame4all.R;
import com.umeng.analytics.MobclickAgent;
import com.umeng.commonsdk.UMConfigure;

public class GameListActivity extends Activity {
//    按钮
    private Button button_1;
    private Button button_2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        赋予控件内容
        setContentView(R.layout.activity_list);
//        给按钮赋值
        button_1 = (Button) findViewById(R.id.button_1);
        button_2 = (Button) findViewById(R.id.button_2);

//        赋予点击事件
        button_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("GameListActivity", "按钮1点击");
                Intent intent = new Intent(GameListActivity.this, EntryActivity.class);
                GameManager.getInstance().runningGame = "ffight";
                GameManager.getInstance().gameName = "FFIGHT";
                startActivity(intent);
                finish();
            }
        });
        button_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Log.d("GameListActivity", "按钮2点击");
                Intent intent = new Intent(GameListActivity.this, EntryActivity.class);
                GameManager.getInstance().runningGame = "zoar";
                GameManager.getInstance().gameName = "ZOAR";
                startActivity(intent);
                finish();
            }
        });

        Log.d("GameListActivity", "准备加载库");
        System.loadLibrary("MAME4all");



        UMConfigure.init(this, "5b0d00f98f4a9d209d00004b", "Umeng", UMConfigure.DEVICE_TYPE_PHONE, "1dc282800cb78bbe9fe7af7306676921");
        MobclickAgent.setSecret(this, "1dc282800cb78bbe9fe7af7306676921");

        MobclickAgent.onProfileSignIn("newgame");
        MobclickAgent.onEvent(this, "开始选择游戏");
    }

    @Override
    protected void onPause() {
        super.onPause();

        MobclickAgent.onPause(this);
        MobclickAgent.onPageEnd("GameListActivity");
    }

    @Override
    protected void onResume() {
        super.onResume();

        MobclickAgent.onResume(this);
        MobclickAgent.onPageStart("GameListActivity");
    }
}
